package main

import (
	"encoding/hex"
	"log"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/blakeroy219/demarco-garbage/mysql"
	"gitlab.com/blakeroy219/demarco-garbage/transport"
)

func main() {
	// Server Setup
	db, err := mysql.Connect()
	if err != nil {
		log.Fatal(err)
	}

	cookieJar := new(http.CookieJar)
	twoFactorCache := make(map[int]string, 0)

	bytes := make([]byte, 0)
	bytes = append(bytes, 97, 98, 109, 12, 33, 44, 56, 67, 75, 128, 129, 180, 200, 220, 225, 228, 240, 255, 1, 5, 7, 24, 4, 157, 22, 28, 0, 35, 66, 197, 201, 33)
	aesKey := hex.EncodeToString(bytes)

	APIHandler := transport.APIHandler{
		DB:        db,
		AccessKey: os.Getenv("ACCESS_KEY"),
		CookieJar: *cookieJar,
		Cache:     twoFactorCache,
		AESKey:    aesKey,
	}

	// Start Server
	log.Println("Starting Server")

	multiplexer := APIHandler.CreateMultiplexer()
	http.ListenAndServe(":8080", multiplexer)

	// Graceful Shutdown
}
