module gitlab.com/blakeroy219/demarco-garbage

go 1.16

require (
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/stripe/stripe-go v70.15.0+incompatible
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce
)
