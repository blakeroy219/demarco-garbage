# internet-orders
Internet Orders is an online ordering system that listens for orders over HTTP. Incoming orders are sent to an admin dashboard where logged in admins can change the status of an order.
