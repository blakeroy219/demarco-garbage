package models

// The card model is used to represent card info

type Card struct {
	CardNumber      string
	NameOnCard      string
	ExpirationMonth string
	ExpirationYear  string
	BillingAddress  string
	BillingZip      string
	BillingState    string
	EncryptionKey   string
}
