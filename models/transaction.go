package models

type Transaction struct {
	Amount int64
	Date   string
	Email  string
}
