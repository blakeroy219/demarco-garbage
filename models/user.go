package models

// The user model is used to represent a user within the datebase

type User struct {
	ID              int
	Email           string
	Password        string
	Active          int
	FullName        string
	PhoneNumber     string
	PendingApproval int
	Address         string
	State           string
	ZipCode         string
	Amount          string
	CardID          int
	CompanyID       int
	IsCompany       bool
}
