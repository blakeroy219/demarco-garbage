package controllers

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"log"

	"gitlab.com/blakeroy219/demarco-garbage/models"
	"gitlab.com/blakeroy219/demarco-garbage/mysql"
)

// Create will receive the body contents of a request
// then store the item in the MySQL Database Server
func CreateCard(card *models.Card, userIdInt int, db *mysql.MySQLDatabase, key string) error {
	cardNumber, err := encrypt(card.CardNumber, key)
	if err != nil {
		return err
	}

	sql := fmt.Sprintf("INSERT INTO card(card_number, name_on_card, expiration_month, expiration_year, billing_address, billing_zip, billing_state) VALUES (%v, %s, %s, %s, %s, %s, %s);",
		fmt.Sprintf("'%s'", cardNumber), fmt.Sprintf("'%s'", card.NameOnCard), fmt.Sprintf("'%s'", card.ExpirationMonth), fmt.Sprintf("'%s'", card.ExpirationYear), fmt.Sprintf("'%s'", card.BillingAddress), fmt.Sprintf("'%s'", card.BillingZip),
		fmt.Sprintf("'%s'", card.BillingState))

	_, err = db.Connection.Exec(sql)
	if err != nil {
		return err
	}

	sql = fmt.Sprintf("SELECT id FROM card WHERE card_number = %s", fmt.Sprintf("'%v'", cardNumber))
	var cardId int
	err = db.Connection.QueryRow(sql).Scan(&cardId)
	if err != nil {
		return err
	}

	sql = fmt.Sprintf("UPDATE user SET CardID = %v WHERE id = %v", fmt.Sprintf("%v", cardId), fmt.Sprintf("'%v'", userIdInt))
	_, err = db.Connection.Exec(sql)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return nil
}

// Read will receive the body contents of a request
// then retreive the specified item in the MySQL Database Server
func ReadCard(cardNumber string, db *mysql.MySQLDatabase, key string) (*models.Card, error) {
	var cardData models.Card

	encryptNumber, err := encrypt(cardNumber, key)
	if err != nil {
		return &cardData, err
	}
	sql := fmt.Sprintf("SELECT card_number FROM card WHERE card_number = %s;", fmt.Sprintf("'%s'", encryptNumber))

	err = db.Connection.QueryRow(sql).Scan(&cardData.CardNumber)
	if err != nil {
		return &cardData, err
	}

	cardNumber, err = Decrypt(cardNumber, key)
	cardData.CardNumber = cardNumber

	return &cardData, nil

}

// Update will receive the body contents of a request
// then update the specified item in the MySQL Database Server
func UpdateCard(card *models.Card, cardId int, db *mysql.MySQLDatabase, key string) error {
	cardNumber, err := encrypt(card.CardNumber, key)
	if err != nil {
		return err
	}

	sql := fmt.Sprintf("UPDATE card SET card_number = %s, name_on_card = %s, expiration_month = %s, expiration_year = %s, billing_address = %s, billing_zip = %s, billing_state = %s WHERE id = %s;",
		fmt.Sprintf("'%v'", cardNumber), fmt.Sprintf("'%s'", card.NameOnCard),
		fmt.Sprintf("'%s'", card.ExpirationMonth), fmt.Sprintf("'%s'", card.ExpirationYear), fmt.Sprintf("'%s'", card.BillingAddress), fmt.Sprintf("'%s'", card.BillingZip),
		fmt.Sprintf("'%s'", card.BillingState), fmt.Sprintf("%v", cardId))

	_, err = db.Connection.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

// Delete will receive the body contents of a request
// then delete the specified item in the MySQL Database Server
func DeleteCard(cardId int, userIdInt int, db *mysql.MySQLDatabase) error {
	sql := fmt.Sprintf("UPDATE user SET CardID = %s WHERE id = %v;", "null", fmt.Sprintf("%v", userIdInt))
	_, err := db.Connection.Exec(sql)
	if err != nil {
		return err
	}

	sql = fmt.Sprintf("DELETE FROM card WHERE id = %v;", cardId)
	_, err = db.Connection.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

func encrypt(stringToEncrypt string, keyString string) (encryptedString string, err error) {
	key, _ := hex.DecodeString(keyString)
	plaintext := []byte(stringToEncrypt)

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}

	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	//Create a nonce. Nonce should be from GCM
	nonce := make([]byte, aesGCM.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", err
	}

	ciphertext := aesGCM.Seal(nonce, nonce, plaintext, nil)
	return fmt.Sprintf("%x", ciphertext), nil
}

func Decrypt(encryptedString string, keyString string) (decryptedString string, err error) {
	enc, _ := hex.DecodeString(encryptedString)
	key, _ := hex.DecodeString(keyString)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	nonceSize := aesGCM.NonceSize()

	nonce, ciphertext := enc[:nonceSize], enc[nonceSize:]

	plaintext, err := aesGCM.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s", plaintext), nil
}
