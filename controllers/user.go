package controllers

import (
	"fmt"
	"log"

	"gitlab.com/blakeroy219/demarco-garbage/models"
	"gitlab.com/blakeroy219/demarco-garbage/mysql"
)

// Createuser will receive the users information as a request
//  and then store the Users information in the user model in the MySQL Database Server
func CreateUser(user *models.User, db *mysql.MySQLDatabase) error {
	sql := fmt.Sprintf("INSERT INTO user(email, password, active, full_name, phone_number) VALUES (%s, %s, %v, '', '')", fmt.Sprintf("'%s'", user.Email), fmt.Sprintf("'%s'", user.Password), fmt.Sprintf("%v", user.Active))
	_, err := db.Connection.Exec(sql)
	if err != nil {
		return err
	}

	return nil
}

// Read will receive the user model contents of a request
// then retreive the specified user model in the MySQL Database Server
func ReadUser(user *models.User, db *mysql.MySQLDatabase) (*models.User, error) {
	sql := fmt.Sprintf("SELECT id, email, password FROM user WHERE email = %s ", fmt.Sprintf("'%s'", user.Email))

	var userData models.User
	err := db.Connection.QueryRow(sql).Scan(&userData.ID, &userData.Email, &userData.Password)
	if err != nil {
		return &userData, err
	}

	return &userData, nil
}

// Update will receive the body contents of a request
// then update the specified item in the MySQL Database Server
func UpdateUser(user *models.User, db *mysql.MySQLDatabase) error {
	var sql string
	log.Println(*user)
	if len(user.Email) > 3 {
		sql = fmt.Sprintf("UPDATE user SET pending_approval = %v WHERE email = %v ", user.PendingApproval, fmt.Sprintf("'%s'", user.Email))
	} else {
		sql = fmt.Sprintf("UPDATE user SET active = %v, pending_approval = %v WHERE id = %v ", user.Active, user.PendingApproval, user.ID)
	}
	log.Println(sql)
	_, err := db.Connection.Exec(sql)
	if err != nil {
		return err
	}
	log.Println("Success")
	return nil
}

func UpdateUserExtraInfo(user *models.User, db *mysql.MySQLDatabase) error {
	sql := fmt.Sprintf("UPDATE user SET full_name = %s, address = %s, state = %s, zip_code = %s, phone_number = %s WHERE id = %v ",
		fmt.Sprintf("'%s'", user.FullName), fmt.Sprintf("'%s'", user.Address), fmt.Sprintf("'%s'", user.State),
		fmt.Sprintf("'%s'", user.ZipCode), fmt.Sprintf("'%s'", user.PhoneNumber), user.ID)
	_, err := db.Connection.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

// Delete will receive the body contents of a request
// then delete the specified item in the MySQL Database Server
func DeleteUser(userIdInt int, db *mysql.MySQLDatabase) error {
	var userData models.User
	sql := fmt.Sprintf("SELECT CardID FROM user WHERE id = %v;", userIdInt)
	err := db.Connection.QueryRow(sql).Scan(&userData.CardID)
	if err != nil {
		return err
	}

	sql = fmt.Sprintf("DELETE FROM user WHERE id = %v;", userIdInt)
	_, err = db.Connection.Exec(sql)
	if err != nil {
		return err
	}

	sql = fmt.Sprintf("DELETE FROM card WHERE id = %v;", userData.CardID)
	_, err = db.Connection.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

func GetUser(userId int, db *mysql.MySQLDatabase) (*models.User, error) {
	sql := fmt.Sprintf("SELECT email, full_name, phone_number, active, pending_approval FROM user WHERE id = %v", userId)

	var userData models.User
	err := db.Connection.QueryRow(sql).Scan(&userData.Email, &userData.FullName, &userData.PhoneNumber, &userData.Active, &userData.PendingApproval)
	if err != nil {
		return &userData, err
	}
	return &userData, nil
}
