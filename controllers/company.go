package controllers

import (
	"fmt"
	"log"

	"gitlab.com/blakeroy219/demarco-garbage/models"
	"gitlab.com/blakeroy219/demarco-garbage/mysql"
)

// Create will receive the body contents of a request
// then store the item in the MySQL Database Server
func CreateCompany(company *models.Company, db *mysql.MySQLDatabase) error {
	sql := fmt.Sprintf("INSERT INTO company(name, password) VALUES (%s, %s);", company.Name, company.Password)

	_, err := db.Connection.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

// Read will receive the body contents of a request
// then retreive the specified item in the MySQL Database Server
func ReadCompany(company *models.Company, db *mysql.MySQLDatabase) (*models.Company, error) {
	sql := fmt.Sprintf("SELECT * FROM card WHERE user_id = %v;", company.ID)

	row := db.Connection.QueryRow(sql)
	var companyData *models.Company

	err := row.Scan(companyData)
	if err != nil {
		return companyData, err
	}

	return companyData, nil
}

// Update will receive the body contents of a request
// then update the specified item in the MySQL Database Server
func UpdateCompany(company *models.Company, db *mysql.MySQLDatabase) error {
	sql := fmt.Sprintf("UPDATE company SET name = %s WHERE id = %v;", company.Name, company.ID)

	_, err := db.Connection.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

// Delete will receive the body contents of a request
// then delete the specified item in the MySQL Database Server
func DeleteCompany(companyId int, db *mysql.MySQLDatabase) error {
	sql := fmt.Sprintf("DELETE FROM company WHERE id = %v;", companyId)

	_, err := db.Connection.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

func GetClients(pendingApproval int, db *mysql.MySQLDatabase) ([]*models.User, error) {
	activeClients := make([]*models.User, 0)

	sql := fmt.Sprintf("SELECT full_name, email, phone_number FROM user WHERE pending_approval = %v AND active = %v", pendingApproval, 1)
	rows, err := db.Connection.Query(sql)
	if err != nil {
		return activeClients, err
	}

	for rows.Next() {
		var tempUser = models.User{}
		if err = rows.Scan(&tempUser.FullName, &tempUser.Email, &tempUser.PhoneNumber); err != nil {
			return activeClients, err
		}
		activeClients = append(activeClients, &tempUser)
		log.Println(activeClients)
	}

	return activeClients, nil
}
