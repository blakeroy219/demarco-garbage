package controllers

import (
	"fmt"
	"strconv"
	"time"

	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/charge"
	"github.com/stripe/stripe-go/token"
	"gitlab.com/blakeroy219/demarco-garbage/models"
	"gitlab.com/blakeroy219/demarco-garbage/mysql"
)

func CreateTransaction(user *models.User, db *mysql.MySQLDatabase, key string) error {
	// Set Stripe API key
	stripe.Key = "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"

	sql := fmt.Sprintf("SELECT CardID FROM user WHERE email = %s", fmt.Sprintf("'%s'", user.Email))
	var userData models.User
	err := db.Connection.QueryRow(sql).Scan(&userData.CardID)
	if err != nil {
		return err
	}

	cardSql := "SELECT name_on_card, card_number, expiration_month, expiration_year, billing_address, billing_zip, billing_state from card WHERE id = %v"

	var card models.Card

	err = db.Connection.QueryRow(fmt.Sprintf(cardSql, userData.CardID)).Scan(
		&card.NameOnCard, &card.CardNumber, &card.ExpirationMonth,
		&card.ExpirationYear, &card.BillingAddress, &card.BillingZip, &card.BillingState,
	)
	cardNumber, err := Decrypt(card.CardNumber, key)
	if err != nil {
		return err
	}
	card.CardNumber = cardNumber

	params := &stripe.TokenParams{
		Card: &stripe.CardParams{
			Currency:     stripe.String("USD"),
			Number:       stripe.String(card.CardNumber),
			AddressLine1: stripe.String(card.BillingAddress),
			AddressState: stripe.String(card.BillingState),
			AddressZip:   stripe.String(card.BillingZip),
			ExpMonth:     stripe.String(card.ExpirationMonth),
			ExpYear:      stripe.String(card.ExpirationYear),
			Name:         stripe.String(card.NameOnCard),
		},
	}
	stripeToken, err := token.New(params)
	if err != nil {
		return err
	}

	amount, err := strconv.ParseInt(user.Amount, 10, 64)
	if err != nil {
		return err
	}

	chargeParams := &stripe.ChargeParams{
		Amount:       stripe.Int64(amount),
		Currency:     stripe.String(string(stripe.CurrencyUSD)),
		Description:  stripe.String(fmt.Sprintf("%s", user.Email)),
		ReceiptEmail: stripe.String(user.Email),
	}

	chargeParams.SetSource(stripeToken.ID)

	_, err = charge.New(chargeParams)
	if err != nil {
		return err
	}

	sql = fmt.Sprintf("INSERT INTO transactions(amount, date, email) VALUES (%v, %s, %s)", amount, fmt.Sprintf("'%s'", time.Now()), fmt.Sprintf("'%s'", user.Email))
	_, err = db.Connection.Exec(sql)
	if err != nil {
		return err
	}

	return nil
}

func GetTransactions(userIdInt int, db *mysql.MySQLDatabase) ([]*models.Transaction, error) {
	transactions := make([]*models.Transaction, 0)
	sql := fmt.Sprintf("SELECT email FROM user WHERE id = %s ", fmt.Sprintf("'%v'", userIdInt))

	var userData models.User
	err := db.Connection.QueryRow(sql).Scan(&userData.Email)
	if err != nil {
		return transactions, err
	}

	sql = fmt.Sprintf("SELECT amount, date FROM transactions WHERE email = %s", fmt.Sprintf("'%s'", userData.Email))
	rows, err := db.Connection.Query(sql)
	if err != nil {
		return transactions, err
	}

	for rows.Next() {
		var tempTransaction = models.Transaction{}
		if err = rows.Scan(&tempTransaction.Amount, &tempTransaction.Date); err != nil {
			return transactions, err
		}
		transactions = append(transactions, &tempTransaction)
	}

	return transactions, nil
}
