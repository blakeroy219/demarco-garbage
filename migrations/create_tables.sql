CREATE TABLE `demarco_disposal`.`card`
(
    id BIGINT AUTO_INCREMENT,
	card_number VARCHAR(255) NOT NULL,
	name_on_card VARCHAR(255) NOT NULL,
	expiration_month VARCHAR(255) NOT NULL,
	expiration_year VARCHAR(255) NOT NULL,
	billing_address VARCHAR(255) NOT NULL,
	billing_zip VARCHAR(255) NOT NULL,
	billing_state VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `demarco_disposal`.`company`
(
    id BIGINT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `demarco_disposal`.`user`
(
    id BIGINT AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
	active BIGINT,
    full_name VARCHAR(255),
    phone_number VARCHAR(255),
    pending_approval BIGINT,
    address VARCHAR(255),
    state VARCHAR(255),
    zip_code VARCHAR(255),
	CardID BIGINT,
	CompanyID BIGINT,
    PRIMARY KEY (`id`),
    FOREIGN KEY (CardID) REFERENCES card(`id`),
    FOREIGN KEY (CompanyID) REFERENCES company(`id`)
);

CREATE TABLE `demarco_disposal`.`transactions`
(
    id BIGINT AUTO_INCREMENT,
    amount VARCHAR(255),
    date VARCHAR(255),
    email VARCHAR(255),
    CompanyID BIGINT,
    PRIMARY KEY (`id`),
    FOREIGN KEY (CompanyID) REFERENCES company(`id`)
);