FROM node:13.12.0-alpine
WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent

# prevent the re-installation of vendors at every change in the source code
COPY . . 

CMD ["npm", "start"]