package transport

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/smtp"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go/v4"
	"gitlab.com/blakeroy219/demarco-garbage/controllers"
	"gitlab.com/blakeroy219/demarco-garbage/models"
	"gitlab.com/blakeroy219/demarco-garbage/mysql"
	"golang.org/x/crypto/bcrypt"
)

type APIHandler struct {
	DB        *mysql.MySQLDatabase
	AccessKey string
	CookieJar http.CookieJar
	Cache     map[int]string
	AESKey    string
}

type TwoFA struct {
	Code string
}

func (handler *APIHandler) HomePage() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.Write([]byte("Server Started"))
		res.WriteHeader(http.StatusOK)
	})
}

func (handler *APIHandler) PaymentInfo() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/json" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}

		if req.Method == http.MethodPost {
			// Decode the card into a struct
			var newCard models.Card
			err := json.NewDecoder(req.Body).Decode(&newCard)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}

			_, err = controllers.ReadCard(newCard.CardNumber, handler.DB, handler.AESKey)
			if err == nil {
				http.Error(res, "A card with this information already exists", http.StatusBadRequest)
				return
			}

			cookie, err := req.Cookie("jwt")
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
				return
			}

			token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
				return []byte(handler.AccessKey), nil
			})
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
			}

			claims := token.Claims.(*jwt.StandardClaims)
			userId := claims.Issuer
			userIdInt, err := strconv.Atoi(userId)

			err = controllers.CreateCard(&newCard, userIdInt, handler.DB, handler.AESKey)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}

			res.WriteHeader(http.StatusCreated)

		} else if req.Method == http.MethodPut {
			// Decode the card into a struct
			var newCard models.Card
			err := json.NewDecoder(req.Body).Decode(&newCard)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}

			_, err = controllers.ReadCard(newCard.CardNumber, handler.DB, handler.AESKey)
			if err == nil {
				http.Error(res, "A card with this information already exists", http.StatusBadRequest)
				return
			}

			cookie, err := req.Cookie("jwt")
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
				return
			}

			token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
				return []byte(handler.AccessKey), nil
			})
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
			}

			claims := token.Claims.(*jwt.StandardClaims)
			userId := claims.Issuer
			userIdInt, err := strconv.Atoi(userId)

			row := handler.DB.Connection.QueryRow(fmt.Sprintf("SELECT card_id FROM user WHERE id = %v;", fmt.Sprintf("%v", userIdInt)))
			var userData *models.User

			err = row.Scan(userData.CardID)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}

			err = controllers.UpdateCard(&newCard, userData.CardID, handler.DB, handler.AESKey)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}

			res.WriteHeader(http.StatusCreated)

		} else if req.Method == http.MethodDelete {
			cookie, err := req.Cookie("jwt")
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
				return
			}

			token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
				return []byte(handler.AccessKey), nil
			})
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
			}

			claims := token.Claims.(*jwt.StandardClaims)
			userId := claims.Issuer
			userIdInt, err := strconv.Atoi(userId)

			row := handler.DB.Connection.QueryRow(fmt.Sprintf("SELECT CardID FROM user WHERE id = %v;", fmt.Sprintf("%v", userIdInt)))
			var userData models.User

			err = row.Scan(&userData.CardID)
			if err != nil {
				log.Println(err.Error())
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}

			log.Println(userData.CardID)

			err = controllers.DeleteCard(userData.CardID, userIdInt, handler.DB)
			if err != nil {
				log.Println(err.Error())
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}

			res.WriteHeader(http.StatusCreated)

		} else {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
			return
		}
	})
}

// RegisterUser is a handler function that is used to create a user in the database
func (handler *APIHandler) RegisterUser() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/json" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}

		if req.Method == http.MethodPost {
			// Decode the user into a struct
			var attemptingUser models.User
			err := json.NewDecoder(req.Body).Decode(&attemptingUser)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}

			var exists models.User
			exists.Email = attemptingUser.Email

			_, err = controllers.ReadUser(&exists, handler.DB)
			if err == nil {
				http.Error(res, "User Exists Under This Email", http.StatusBadRequest)
				return
			}

			password, _ := bcrypt.GenerateFromPassword([]byte(attemptingUser.Password), 14)

			attemptingUser.Password = string(password)
			attemptingUser.Active = 0

			// Create user in MySQL
			err = controllers.CreateUser(&attemptingUser, handler.DB)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}

			res.WriteHeader(http.StatusCreated)

		} else {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
		}
	})
}

func (handler *APIHandler) LoginUser() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/json" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}

		// Login attempt from react client
		if req.Method == http.MethodPost {
			// Decode the user into a struct
			var attemptingUser models.User
			err := json.NewDecoder(req.Body).Decode(&attemptingUser)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}

			// Retreive user data from MySQL
			userData, err := controllers.ReadUser(&attemptingUser, handler.DB)
			if err != nil {
				http.Error(res, "Invalid Email or Password", http.StatusBadRequest)
				return
			}

			expDate := time.Now().Add(time.Hour * 24).Unix()

			// In the case we have a valid email and password, the user is granted access
			if attemptingUser.Email == userData.Email {
				if bcrypt.CompareHashAndPassword([]byte(userData.Password), []byte(attemptingUser.Password)) == nil {
					claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
						Issuer:    strconv.Itoa(userData.ID),
						ExpiresAt: jwt.NewTime(float64(expDate)),
					})

					token, err := claims.SignedString([]byte(handler.AccessKey))
					if err != nil {
						http.Error(res, err.Error(), http.StatusInternalServerError)
					}

					cookie := &http.Cookie{
						Name:     "jwt",
						Value:    token,
						Expires:  time.Now().Add(time.Hour * 24),
						HttpOnly: true,
					}
					http.SetCookie(res, cookie)

					// Sender data.
					from := "2fademarcodisposal@gmail.com"
					password := "DemarcoDisposal2FA"

					// Receiver email address.
					to := []string{attemptingUser.Email}

					// smtp server configuration.
					smtpHost := "smtp.gmail.com"

					// Message.
					var authCode = randString()
					message := []byte("Subject: DeMarco Disposal Two Factor Authentication\r\n" + "\r\nAuth code expires in 5 minutes - " + authCode + "\r\n")

					// Authentication.
					auth := smtp.PlainAuth("", from, password, smtpHost)

					// Sending email.
					err = smtp.SendMail(smtpHost+":587", auth, from, to, message)
					if err != nil {
						http.Error(res, err.Error(), http.StatusInternalServerError)
						return
					}

					handler.Cache[userData.ID] = authCode

					res.WriteHeader(http.StatusOK)

				} else {
					// Password did not match
					http.Error(res, "Invalid Email or Password", http.StatusUnauthorized)
					return
				}
			} else {
				// Email not found
				http.Error(res, "Invalid Email or Password", http.StatusUnauthorized)
			}
		} else {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
		}
	})
}

func (handler *APIHandler) UpdateUser() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/json" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}

		if req.Method == http.MethodPut {
			// Decode the user into a struct
			var attemptingUser models.User
			err := json.NewDecoder(req.Body).Decode(&attemptingUser)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}
			cookie, err := req.Cookie("jwt")
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
				return
			}

			token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
				return []byte(handler.AccessKey), nil
			})
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
			}

			claims := token.Claims.(*jwt.StandardClaims)
			userId := claims.Issuer
			userIdInt, err := strconv.Atoi(userId)

			attemptingUser.ID = userIdInt

			if len(attemptingUser.PhoneNumber) > 3 {
				err = controllers.UpdateUserExtraInfo(&attemptingUser, handler.DB)
			} else {
				err = controllers.UpdateUser(&attemptingUser, handler.DB)
			}

			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}

		} else {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
		}
	})
}

func (handler *APIHandler) DeleteUser() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/json" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}

		if req.Method == http.MethodDelete {
			cookie, err := req.Cookie("jwt")
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
				return
			}

			token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
				return []byte(handler.AccessKey), nil
			})
			if err != nil {
				http.Error(res, err.Error(), http.StatusUnauthorized)
			}

			claims := token.Claims.(*jwt.StandardClaims)
			userId := claims.Issuer
			userIdInt, err := strconv.Atoi(userId)

			err = controllers.DeleteUser(userIdInt, handler.DB)

			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
		}
	})
}

func (handler *APIHandler) ChargeClients() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/json" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}

		if req.Method == http.MethodPost {
			var user models.User
			err := json.NewDecoder(req.Body).Decode(&user)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
			}
			err = controllers.CreateTransaction(&user, handler.DB, handler.AESKey)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
		}
	})
}

func (handler *APIHandler) UpdateCompany() http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/x-www-form-urlencoded" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}
		if req.Method == http.MethodPut {
			req.ParseForm()
			// Decode the user into a struct
			var attemptingCompany models.Company

			attemptingCompany.Name = fmt.Sprintf("'%s'", req.FormValue("name"))
			err := controllers.UpdateCompany(&attemptingCompany, handler.DB)

			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
		}
	})
}

func (handler *APIHandler) DeleteCompany() http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/x-www-form-urlencoded" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}
		if req.Method == http.MethodDelete {
			req.ParseForm()
			companyId, err := strconv.Atoi(req.FormValue("companyId"))

			err = controllers.DeleteCompany(companyId, handler.DB)
			if err != nil {
				http.Error(res, err.Error(), http.StatusInternalServerError)
				return
			}

		} else {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
		}
	})
}

func (handler *APIHandler) GetUser() http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		cookie, err := req.Cookie("jwt")
		if err != nil {
			http.Error(res, err.Error(), http.StatusUnauthorized)
			return
		}

		token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
			return []byte(handler.AccessKey), nil
		})
		if err != nil {
			http.Error(res, err.Error(), http.StatusUnauthorized)
		}

		claims := token.Claims.(*jwt.StandardClaims)
		userId := claims.Issuer
		userIdInt, err := strconv.Atoi(userId)

		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

		user, err := controllers.GetUser(userIdInt, handler.DB)
		if strings.ToLower(user.Email) == "demarco.disposal518@gmail.com" {
			user.IsCompany = true
		}

		err = json.NewEncoder(res).Encode(user)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

		return
	})
}
func (handler *APIHandler) TwoFA() http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		cookie, err := req.Cookie("jwt")
		if err != nil {
			http.Error(res, err.Error(), http.StatusUnauthorized)
			return
		}
		token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
			return []byte(handler.AccessKey), nil
		})
		if err != nil {
			http.Error(res, err.Error(), http.StatusUnauthorized)
			return
		}

		claims := token.Claims.(*jwt.StandardClaims)
		userId := claims.Issuer
		userIdInt, err := strconv.Atoi(userId)

		var sentCode TwoFA
		json.NewDecoder(req.Body).Decode(&sentCode)

		if authCode, present := handler.Cache[userIdInt]; !present {
			http.Error(res, err.Error(), http.StatusUnauthorized)
			return
		} else {
			if sentCode.Code == authCode {
				delete(handler.Cache, userIdInt)
				res.WriteHeader(http.StatusOK)
			} else {
				http.Error(res, "Auth Code Invalid", http.StatusUnauthorized)
				return
			}
		}
	})

}

func (handler *APIHandler) GetTransactions() http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/json" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}

		cookie, err := req.Cookie("jwt")
		if err != nil {
			http.Error(res, err.Error(), http.StatusUnauthorized)
			return
		}

		token, err := jwt.ParseWithClaims(cookie.Value, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
			return []byte(handler.AccessKey), nil
		})
		if err != nil {
			http.Error(res, err.Error(), http.StatusUnauthorized)
		}

		claims := token.Claims.(*jwt.StandardClaims)
		userId := claims.Issuer
		userIdInt, err := strconv.Atoi(userId)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

		transactions, err := controllers.GetTransactions(userIdInt, handler.DB)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

		err = json.NewEncoder(res).Encode(transactions)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

	})
}

func (handler *APIHandler) GetClients() http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Type") != "application/json" {
			res.WriteHeader(http.StatusUnsupportedMediaType)
			http.Error(res, "Unsupported Media Type", http.StatusBadRequest)
			return
		}
		if req.Method != http.MethodPost {
			http.Error(res, "Unsupported HTTP Method", http.StatusBadRequest)
		}

		var attemptingUser models.User
		err := json.NewDecoder(req.Body).Decode(&attemptingUser)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

		clients, err := controllers.GetClients(attemptingUser.PendingApproval, handler.DB)
		err = json.NewEncoder(res).Encode(clients)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

	})
}

func (handler *APIHandler) CreateMultiplexer() *http.ServeMux {
	multiplexer := http.NewServeMux()
	multiplexer.Handle("/", handler.HomePage())
	multiplexer.Handle("/register", handler.RegisterUser())
	multiplexer.Handle("/login", handler.LoginUser())
	multiplexer.Handle("/user/update", handler.UpdateUser())
	multiplexer.Handle("/user/delete", handler.DeleteUser())
	multiplexer.Handle("/user/payments", handler.PaymentInfo())
	multiplexer.Handle("/company/charge", handler.ChargeClients())
	multiplexer.Handle("/company/update", handler.UpdateCompany())
	multiplexer.Handle("/company/delete", handler.DeleteCompany())
	multiplexer.Handle("/user/get", handler.GetUser())
	multiplexer.Handle("/user/transactions", handler.GetTransactions())
	multiplexer.Handle("/company/getClients", handler.GetClients())
	multiplexer.Handle("/user/auth", handler.TwoFA())

	return multiplexer
}

func randString() string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	Value1 := r.Perm(6)
	var tempValue = ""
	for i := 0; i < 6; i++ {
		tempValue = strconv.Itoa(Value1[i]) + tempValue
	}
	return tempValue
}
