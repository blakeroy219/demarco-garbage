package transport_test

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"gitlab.com/blakeroy219/demarco-garbage/mysql"
	"gitlab.com/blakeroy219/demarco-garbage/transport"
)

func TestHomePage(t *testing.T) {
	req := httptest.NewRequest("GET", "/", nil)
	handler := transport.APIHandler{}

	rr := httptest.NewRecorder()
	handlerFunc := handler.HomePage()

	handlerFunc.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func TestPaymentInfo(t *testing.T) {
	t.Run("Unsupported Method", func(t *testing.T) {
		req := httptest.NewRequest("DELETE", "/", nil)
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		handler := transport.APIHandler{}

		rr := httptest.NewRecorder()
		handlerFunc := handler.PaymentInfo()

		handlerFunc.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("got %v want %v",
				status, http.StatusBadRequest)
		}
	})
	t.Run("No userId", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/", nil)
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		db, err := mysql.Connect()
		if err != nil {
			t.Fatal(err)
		}

		handler := transport.APIHandler{DB: db}

		rr := httptest.NewRecorder()
		handlerFunc := handler.PaymentInfo()

		handlerFunc.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("got %v want %v",
				status, http.StatusBadRequest)
		}
	})
	t.Run("Payment Saved", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/", nil)

		form, _ := url.ParseQuery(req.URL.RawQuery)
		form.Add("userId", "1")
		form.Add("cardNumber", "0000000000000000")
		req.URL.RawQuery = form.Encode()

		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		db, err := mysql.Connect()
		if err != nil {
			t.Fatal(err)
		}

		handler := transport.APIHandler{DB: db}

		rr := httptest.NewRecorder()
		handlerFunc := handler.PaymentInfo()

		handlerFunc.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusCreated {
			t.Errorf("got %v want %v",
				status, http.StatusCreated)
		}
	})
	t.Run("Payment Updated", func(t *testing.T) {
		req := httptest.NewRequest("PUT", "/", nil)

		form, _ := url.ParseQuery(req.URL.RawQuery)
		form.Add("userId", "1")
		form.Add("cardNumber", "0000000000000000")
		req.URL.RawQuery = form.Encode()

		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		db, err := mysql.Connect()
		if err != nil {
			t.Fatal(err)
		}

		handler := transport.APIHandler{DB: db}

		rr := httptest.NewRecorder()
		handlerFunc := handler.PaymentInfo()

		handlerFunc.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusOK {
			t.Errorf("got %v want %v",
				status, http.StatusOK)
		}
	})
}

func TestRegisterUser(t *testing.T) {
	t.Run("Unsupported Method", func(t *testing.T) {
		req := httptest.NewRequest("DELETE", "/", nil)
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		handler := transport.APIHandler{}

		rr := httptest.NewRecorder()
		handlerFunc := handler.RegisterUser()

		handlerFunc.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("got %v want %v",
				status, http.StatusBadRequest)
		}
	})
	t.Run("User Created", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/user/create", nil)

		form, _ := url.ParseQuery(req.URL.RawQuery)
		form.Add("email", "test@test.com")
		form.Add("password", "password")
		form.Add("passwordConfirm", "password")
		req.URL.RawQuery = form.Encode()

		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		db, err := mysql.Connect()
		if err != nil {
			t.Fatal(err)
		}

		handler := transport.APIHandler{DB: db}

		rr := httptest.NewRecorder()
		handlerFunc := handler.RegisterUser()

		handlerFunc.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusCreated {
			t.Errorf("got %v want %v",
				status, http.StatusCreated)
		}
	})
	t.Run("Passwords Do Not Match", func(t *testing.T) {
		req := httptest.NewRequest("POST", "/user/create", nil)

		form, _ := url.ParseQuery(req.URL.RawQuery)
		form.Add("email", "test@test.com")
		form.Add("password", "password")
		form.Add("passwordConfirm", "NOTpassword")
		req.URL.RawQuery = form.Encode()

		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		db, err := mysql.Connect()
		if err != nil {
			t.Fatal(err)
		}

		handler := transport.APIHandler{DB: db}

		rr := httptest.NewRecorder()
		handlerFunc := handler.RegisterUser()

		handlerFunc.ServeHTTP(rr, req)

		if status := rr.Code; status != http.StatusBadRequest {
			t.Errorf("got %v want %v",
				status, http.StatusBadRequest)
		}
	})
}
