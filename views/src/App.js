import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Welcome from './components/dashboard/welcome';

function App() {
  return (
    <div className='App'>
      <Welcome />     
    </div>
  );
}

export default App;
