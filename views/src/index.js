import React from 'react';

import ReactDOM from 'react-dom';
import './index.css';
import App from './App'; 

import reportWebVitals from './reportWebVitals';
import { BrowserRouter} from "react-router-dom";
import { Route, Routes } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";
import Login from './components/dashboard/login';
import Register from './components/dashboard/register';
import Payment from './components/dashboard/payment';
import UserInfo from './components/dashboard/user_info';
import User from './components/user';
import Company from './components/company';
import Management from './components/dashboard/management';
import Settings from './components/dashboard/settings';
import PostRegUser from './components/dashboard/post_reg_user';
import PostRegComp from './components/dashboard/post_reg_comp';
import AddCard from './components/dashboard/add_card';
import Logout from './components/dashboard/logout'



ReactDOM.render(
  <BrowserRouter>
    <Routes>                
      <Route path='/' element={<App />}/>  
      <Route path='register' element={<Register />}/>               
      <Route path='login' element={<Login />}/>
      <Route path='user' element={<User />}/>
      <Route path='company' element={<Company />}/>
      <Route path='payment' element={<Payment />}/>
      <Route path='user_info' element={<UserInfo />}/>
      <Route path='pending_clients' element={<PostRegComp />}/>
      <Route path='management' element={<Management />}/>
      <Route path='settings' element={<Settings />}/>
      <Route path='update_user' element={<PostRegUser />}/> 
      <Route path='post_reg_comp' element={<PostRegComp />}/>
      <Route path='card' element={<AddCard />}/>
      <Route path='logout' element={<Logout />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
