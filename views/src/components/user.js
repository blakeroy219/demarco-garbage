import { useState } from "react";
import { useLocation } from "react-router-dom";
import UserWelcome from "./dashboard/user_welcome"

function User() {
  const location = useLocation();
  var passedemail = location.state.Email

  const[email, setEmail] = useState(passedemail)

  if(!email){
    getUser()
  }

  async function getUser(){
    await fetch('user/get', {method: 'GET'}).then(response => response.json())
    .then(json => {
        setEmail(json.Email)
    })
  }

    return (
      <div className='User'>
        <UserWelcome Email = {email} />
      </div>
    );
  }
  
  export default User;