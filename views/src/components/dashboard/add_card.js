import Header from "../user_header"
import Footer from "../footer"
import PopupAlert from "./popup_alert"
import { useState } from "react"
import { useLocation } from "react-router-dom"

function AddCard(props) {
    const [popupShow, popupSetShow] = useState(false)
    const [popupType, popupSetType] = useState(false)
    const [popupText, popupSetText] = useState(false)

    const location = useLocation();
    var method = location.state.method

    function card(){
        var name = document.getElementById("nameOnCard")
        var number = document.getElementById("cardNumber")
        var month = document.getElementById("expirationMonth")
        var year = document.getElementById("expirationYear")
        var address = document.getElementById("billingAddress")
        var state = document.getElementById("billingState")
        var zip = document.getElementById("billingZip")
    

        fetch('/user/payments', 
        {
          method: method,
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify({
            NameOnCard: name.value,
            CardNumber: number.value,
            ExpirationMonth: month.value,
            ExpirationYear: year.value,
            BillingAddress: address.value,
            BillingState: state.value,
            BillingZip: zip.value
          })
        }).then((response) =>{
            if(!response.ok){
                response.text().then(text => {
                    popupSetShow(true)
                    popupSetType(1)
                    popupSetText(text)
                  })
            }
            else{
                popupSetShow(true)
                popupSetType(2)
                popupSetText('Successful Creation of Card!')
            }
        })
    };
    return (
    <div>
    <Header />
    <div class="container p-5 " color="dark">
    <div class="card px-4">
        <p class="h8 py-3">Payment Details</p>
        <div class="row gx-3">
            <div class="col-12">
                <div class="d-flex flex-column">
                    <p class="text mb-1">Name on Card</p> <input class="form-control mb-3" type="text" placeholder="Name" id="nameOnCard" />
                </div>
            </div>
            <div class="col-12">
                <div class="d-flex flex-column">
                    <p class="text mb-1">Card Number</p> <input class="form-control mb-3" type="text" placeholder="0000000000000000" id="cardNumber" />
                </div>
            </div>
            <div class="col-6">
                <div class="d-flex flex-column">
                    <p class="text mb-1">Exp Month</p> <input class="form-control mb-3" type="text" placeholder="01" id="expirationMonth" />
                </div>
            </div>
            <div class="col-6">
            <div class="d-flex flex-column">
                    <p class="text mb-1">Exp Year</p> <input class="form-control mb-3" type="text" placeholder="2030" id='expirationYear' />
                </div>
            </div>
            <div class="col-12">
                <div class="d-flex flex-column">
                    <p class="text mb-1">Billing Address</p> <input class="form-control mb-3 pt-2 " type="text" placeholder="123 Billing Address Drive" id="billingAddress" />
                </div>
            </div>
            <div class="col-6">
                <div class="d-flex flex-column">
                    <p class="text mb-1">State</p> <input class="form-control mb-3 pt-2 " type="text" placeholder="NY" id="billingState" />
                </div>
            </div>
            <div class="col-6">
                <div class="d-flex flex-column">
                    <p class="text mb-1">Zip Code</p> <input class="form-control mb-3 pt-2 " placeholder="90876" id="billingZip" />
                </div>
            </div>
            <div class="col-12">
                <div class="btn btn-primary mb-3" onClick={card}> <span class="ps-3">Submit</span> </div>
            </div>
        </div>
        <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
    </div>
    </div>
    <Footer/>
    </div>
)}

export default AddCard