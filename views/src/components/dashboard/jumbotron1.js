
function Jumbotron1(props) {
    return <div>
        <div style={{textAlignVertical: "center",textAlign: "center",backgroundColor: "#3E3E3E",}}class="container-fluid text-light p-5 justify-text-center">
            <div class="container p-5">
                <h1 class="display-1">{props.Header}</h1>
                <hr />
                <p >{props.Body}</p>
            </div>
        </div>
    </div>
}
export default Jumbotron1;