import Header from "../company_header"
import Footer from "../footer"
import Jumbotron1 from "./jumbotron1";
import { useState } from "react";
import PopupAlert from "./popup_alert";
import ChargeClientComponent from "./charge_client_component";

function Management() {  
    const [popupShow, popupSetShow] = useState(false)
    const [popupType, popupSetType] = useState(false)
    const [popupText, popupSetText] = useState(false)
    const [clients, clientSetText] = useState(null)
  
    if (!clients) fetchClients()

    function fetchClients(){
        fetch('/company/getClients', 
        {
          method: 'POST',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify({
            PendingApproval: 0, 
          }),
        }).then((response) =>{
          if(!response.ok){
            response.text().then(text => {
              popupSetShow(true)
              popupSetType(1)
              popupSetText(text)
            })
          }
          else{
            return response.json()
            .then((data) => {
              var client = []
              for(let i = 0; i < data.length; i++){
                var obj = <p><div class='col text-center'><ChargeClientComponent user = {data[i]} /></div></p>
                client.push(obj)
              }
              clientSetText(client)
            })
          }
        })
    }
    return <div className="headermatch">
        <Header/>
        <Jumbotron1 Header = 'Client Management' Body = 'Adjust Amounts Per Individual and Charge' />
        <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="text-center text-lg-start mt-4 pt-2">
        <div>{clients}</div>
        </div>
        </div>
        <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
        <Footer/>
    </div>
    }
    export default Management;


      