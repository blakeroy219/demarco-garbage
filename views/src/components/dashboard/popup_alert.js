import { Alert, Button } from "react-bootstrap"

function PopupAlert (props){
    switch(props.type){
        case(0):
            return <div></div>
        case(1):
            return <div>
            <Alert {...props} variant="danger">
            <Alert.Heading>Something Went Wrong!</Alert.Heading>
            <p>
                {props.text}
            </p>
            <hr />
            <div className="d-flex justify-content-end">
            <Button className='pull-right' variant='danger' onClick={props.onHide}>Close</Button>
            </div>
            </Alert>
            </div>
            case(2):
                return <div>
                    <Alert {...props} variant="success" >
                    <Alert.Heading>Success!</Alert.Heading>
                    <p>
                        {props.text}
                    </p>
                    <hr />
                    <div className="d-flex justify-content-end">
                    <Button className='pull-right' variant='danger' onClick={props.onHide}>Close</Button>
                    </div>
                     </Alert>
                </div>
            default:
                return <div></div>
            }
}
export default PopupAlert