import { Card } from "react-bootstrap";
import CardHeader from "react-bootstrap/esm/CardHeader";
function Transaction(props){
    var formattedAmount = '$';
    var str = props.transaction.Amount.toString()
    for(var i = 0; i <str.length; i++){
        if (i === str.length - 2){
            formattedAmount += '.'
        }
        formattedAmount += str[i]
    }

    var formattedDate = props.transaction.Date.toString().substring(0,23)
    return(
        <Card className="text-center" border='secondary' bg='dark' text='light' style={{ width: '24rem' }}>
        <div>
            <CardHeader>
            Amount <div>{formattedAmount}</div>
            </CardHeader>
            Date <div>{formattedDate}</div>
        </div>
        </Card>
        );
}
export default Transaction