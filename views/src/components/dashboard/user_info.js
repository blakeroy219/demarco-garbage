import InfoTable from "./info_table";
import Header from "../user_header"
import Footer from "../footer"
import Jumbotron1 from './jumbotron1';

function UserInfo() {
    return <div>
    <Header/>
    <Jumbotron1 Header = 'Welcome to the User Info Page' Body = 'This is your info'/>
    <InfoTable/>
    <Footer/>
  </div>
  
}
export default UserInfo