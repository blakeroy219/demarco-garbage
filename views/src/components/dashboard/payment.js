import UserHeader from "../user_header"
import Footer from "../footer"
import React from "react"
import Jumbotron1 from "./jumbotron1"
import Jumbotron2 from "./jumbotron2"
import { useLocation } from "react-router-dom"
import PopupAlert from "./popup_alert"
import { useState } from "react"
import Transaction from "./transaction"

function Payments() {
  const location = useLocation();
  const [popupShow, popupSetShow] = useState(false)
  const [popupType, popupSetType] = useState(false)
  const [popupText, popupSetText] = useState(false)
  const [transactions, setTransactions] = useState(null)

  if (!transactions) getTransactions()

  function getTransactions(){
    fetch('/user/transactions', {method: 'GET', headers: {'Content-Type': 'application/json'}}).then((response) =>{
      if(!response.ok){
          response.text().then(text => {
              popupSetShow(true)
              popupSetType(1)
              popupSetText(text)
            })
        }
      else{
        return response.json()
        .then(data => {
          var transactionsResponses = []
          for(let i = 0; i < data.length; i++){
            var obj = <p><Transaction transaction = {data[i]}/></p>
            transactionsResponses.push(obj)
          }
          setTransactions(transactionsResponses)
        })
      }
      })
  }

  var email = location.state.Email
    return <div>
    <UserHeader Email = {email} />
    <Jumbotron1 Header = 'Payments'/>
    <Jumbotron2 Header = 'Previous Transactions' Body={transactions}/>
    <PopupAlert Show = {popupShow} Type = {popupType} Text = {popupText} onHide={() => popupSetShow(false)} />
  <Footer/>
  </div>
  
}

export default Payments;