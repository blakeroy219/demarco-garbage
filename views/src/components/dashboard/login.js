import { Button,Modal } from "react-bootstrap"
import Header from "../header"
import Footer from "../footer"
import {AiOutlineForm} from "react-icons/ai"
import {useNavigate } from 'react-router-dom';
import { useState } from "react";
import PopupAlert from "./popup_alert";


function Login() {
  const navigate = useNavigate();
  const [popupShow, popupSetShow] = useState(false)
  const [popupType, popupSetType] = useState(false)
  const [popupText, popupSetText] = useState(false)

  const [modalShow, modalsetShow] = useState(false)

  async function loginStatus(){
    var email = document.getElementById("email")
    var password = document.getElementById("password")

    fetch('/login', 
    {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        Email: email.value,
        Password: password.value
      })
    })

    .then((response) =>{
      if(!response.ok){
        response.text().then(text => {
          popupSetShow(true)
          popupSetType(1)
          popupSetText(text)
        })
      }
      else{
        console.log("LOGGED IN SUCESSFULLY")
          modalsetShow(true)
      }
  })
}

  const handleregisterRoute = () =>{ 
      navigate('/register');
  }
  async function TwoFactor(){
    var authCode = document.getElementById("authcode")
    fetch('user/auth', {method:'POST', body: JSON.stringify({
      Code: authCode.value
    })})
    .then((response) =>{
    if(!response.ok){
    response.text().then(text => {
    popupSetShow(true)
    popupSetType(1)
    popupSetText(text)
    })
    }
    else{
    fetch('user/get', {method: 'GET'}).then(response => response.json())
    .then(json => {
    var isCompany = json.IsCompany
    if (isCompany){
    navigate('/company', {state:{Email:json.Email}})
    }
    else{
    navigate('/user', {state:{Email:json.Email}})
    }
    })
    }
    })
   }

   function showModal(){
    modalsetShow(false)
   }

    return <div>
    <Header/>
    <section class="vh-100">
      <div className="headermatch" >
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
          <h1 class="display-1">Login</h1>
            <p>Gain Acess to Our Service in Minutes</p>
              <div class="form-outline mb-4">
                <input type="email" id="email" class="form-control form-control-lg"
                  placeholder="Enter a valid email address" name="email" />
                <label class="form-label" for="email">Email address</label>
              </div>
    
              <div class="form-outline mb-3">
                <input type="password" id="password" class="form-control form-control-lg"
                  placeholder="Enter password" name="password" />
                <label class="form-label" for="password">Password</label>
              </div>
    
              <div class="d-flex justify-content-between align-items-center">
                <div class="form-check mb-0">
                  <input class="form-check-input me-2" type="checkbox" value="" id="form2Example3" />
                  <label class="form-check-label" for="form2Example3">
                    Remember me
                  </label>
                </div>
                <a href="./forgotpassword" class="text-body">Forgot password?</a>
              </div>
    
              <div class="text-center text-lg-start mt-4 pt-2">
                <Button type="submit" onClick={loginStatus} class="btn btn-primary btn-lg"> <AiOutlineForm/> Login</Button>
                <p class="small fw-bold mt-2 pt-1 mb-0">Don't have an account? <Button variant="link" onClick={handleregisterRoute}> Register</Button></p>
              </div>
              <Modal
          show= {modalShow}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          >
          <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
          <h1>An authentication code has been sent to your email</h1>
          </Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <h3>Enter Code Here</h3>
            <input id="authcode" placeholder="authcode" />
          </Modal.Body>
          <Modal.Footer>
          <Button variant='danger' onClick={showModal}>Close</Button>
          <Button variant='primary' onClick={TwoFactor}>Update</Button>
          </Modal.Footer>
          </Modal>
              <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
          </div>
        </div>
      </div>
      <div>
    <div>
    </div>
    </div>
    <Footer/>
    </section>
    </div>
}
export default Login