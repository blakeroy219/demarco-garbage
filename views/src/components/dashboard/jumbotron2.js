

function Jumbotron2(props) {
    return <div>
        <div style={{textAlignVertical: "center",textAlign: "center",backgroundColor: "#212121",}}class="container-fluid text-light p-5">
        <div class="container p-5">
            <h1 class="display-1">{props.Header}</h1>
            <hr />
            <p>{props.Body}</p>   
            <div className="mb-2">
            </div>
        </div>
    </div>
    </div>
}
export default Jumbotron2;