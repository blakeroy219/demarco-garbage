import { Card, Button } from 'react-bootstrap';
import PopupAlert from './popup_alert';
import { useState } from 'react';

//username email phone number  text area for charge
const ChargeClientComponent = (props) => {
        const [popupShow, popupSetShow] = useState(false)
        const [popupType, popupSetType] = useState(false)
        const [popupText, popupSetText] = useState(false)
        const [showCard, setShowCard] = useState(true)


        function chargeClients(){
            fetch('/company/charge', 
            {
              method: 'POST',
              headers: {'Content-Type': 'application/json'},
              body: JSON.stringify({
                Email: props.user.Email,
                Amount: document.getElementById('amount').value,
              })
            })
            .then((response) =>{
              if(!response.ok){
                response.text().then(text => {
                    popupSetShow(true)
                    popupSetType(1)
                    popupSetText(text)
                })
              }
              else{
                    popupSetShow(true)
                    popupSetType(2)
                    popupSetText('Successful Charge!')
                    setShowCard(false)
                }
            })
          }
        return(
        <div>
        <Card className="text-center" show={showCard} border='secondary' bg='dark' text='light' style={{ width: '24rem' }}>
                <Card.Header><h2>{props.user.FullName}</h2></Card.Header>
                <Card.Body>
                <Card.Title>{props.user.Email}</Card.Title>
                <Card.Text>
                        {props.user.PhoneNumber}
                </Card.Text>
                <input id='amount' placeholder='Charge Amount'></input>
                </Card.Body>
                <Card.Footer>
                        <Button variant="primary" onClick={chargeClients}>Charge</Button>
                </Card.Footer>
        </Card>
        <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
        </div>
      );
}
export default ChargeClientComponent;