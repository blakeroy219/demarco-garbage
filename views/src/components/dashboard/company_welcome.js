import Header from "../company_header"
import Footer from "../footer"
import Jumbotron1 from "./jumbotron1";

function CompanyWelcome(props) {
    var display = 'Welcome ' + props.Email
    return <div>
        <Header Email = {props.Email} />
        <Jumbotron1 Header = {display} Body = 'Welcome Megan and Dalton. Here you can charge your active clients, approve incoming clients, and view previous transactions.'/>
        <Footer/>
    </div>
}

export default CompanyWelcome;