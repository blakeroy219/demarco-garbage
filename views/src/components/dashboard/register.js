import { Button } from "react-bootstrap"
import Footer from "../footer"
import Header from "../header"
import {useNavigate } from 'react-router-dom';
import React from 'react';
import {AiOutlineForm} from "react-icons/ai"
import { useState } from "react";
import PopupAlert from "./popup_alert";


function Register() {
  const navigate = useNavigate();
  const [popupShow, popupSetShow] = useState(false)
  const [popupType, popupSetType] = useState(false)
  const [popupText, popupSetText] = useState(false)

  async function registerStatus(){
    var email = document.getElementById("email")
    var password = document.getElementById("password")
    var passwordC = document.getElementById("passwordConfirm")

    if(password.value !== passwordC.value){
      popupSetShow(true)
      popupSetType(1)
      popupSetText('Passwords do not match')
      return
    }

    fetch('/register', 
    {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        Email: email.value,
        Password: password.value,
        PasswordConfirm: passwordC.value
      })
    }).then((response) =>{
      if(!response.ok){
        response.text().then(text => {
          popupSetShow(true)
          popupSetType(1)
          popupSetText(text)
        })
      }
      else{
        popupSetShow(true)
        popupSetType(2)
        popupSetText("Registered Succesfully!")
          }
        })
    }

const handleloginRoute = () =>{ 
  navigate("/login");
}
return <div>
<Header/>
<section class="vh-100">
  <div className="headermatch">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
      <h1 class="display-1">Register</h1>
        <p>Gain Acess to Our Service in Minutes</p>


        <div class="form-outline mb-3">
        <input type="email" id="email" class="form-control form-control-lg"
                  placeholder="Enter a valid email address" name="email" />
                <label class="form-label" for="email">Email address</label>
              </div>
    
              <div class="form-outline mb-3">
                <input type="password" id="password" class="form-control form-control-lg"
                  placeholder="Enter password" name="password" />
                <label class="form-label" for="password">Password</label>
              </div>

              <div class="form-outline mb-3">
                <input type="password" id="passwordConfirm" class="form-control form-control-lg"
                  placeholder="Enter password" name="password" />
                <label class="form-label" for="password">Password</label>
              </div>


          <div class="text-center text-lg-start mt-4 pt-2">
            <Button type="submit" class="btn btn-primary btn-lg" onClick={registerStatus}> <AiOutlineForm/> Register</Button>
            <p class="small fw-bold mt-2 pt-1 mb-0"> Already have an account? <Button variant="link" onClick={handleloginRoute}> Login</Button> </p>
          </div>
          <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
      </div>
    </div>
  </div>
  <Footer/>
</section>
</div>
}
export default Register