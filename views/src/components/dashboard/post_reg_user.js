import { Button } from "react-bootstrap"
import {AiFillCheckCircle} from "react-icons/ai"
import PopupModal from "./popup_modal";
import PopupAlert from "./popup_alert";
import { useState } from "react";
import UserHeader from "../user_header";




function PostRegUser()  {
  const [popupShow, popupSetShow] = useState(false)
  const [popupType, popupSetType] = useState(false)
  const [popupText, popupSetText] = useState(false)
  const [popupModal, setPopupModal] = useState(false)
  const [modalBody, setBody] = useState('')

    function updateInfo(){
      var address = document.getElementById("address")
      var state = document.getElementById("state")
      var zip = document.getElementById("zip")
      var phone = document.getElementById("phone")
      var phoneC = document.getElementById("phoneConfirm")
      var fullName = document.getElementById("fullName")

      if(!address.value || !state.value|| !zip.value|| !phone.value|| !phoneC.value || !fullName.value){
        popupSetShow(true)
        popupSetType(1)
        popupSetText('One or more fields is missing')
        return
      }

      if(phone.value !== phoneC.value){
        popupSetShow(true)
        popupSetType(1)
        popupSetText('Phone Numbers Do not Match')
        return
      }

      setBody({
        Address: address.value,
        State: state.value,
        ZipCode: zip.value,
        PhoneNumber: phone.value,
        FullName: fullName.value
      })

      setPopupModal(true)
    }
return <div>
  <UserHeader />
  <div class="container-fluid bg-dark text-light">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
      <h1>Update Information</h1>
        <p>Please Fill in All Fields</p>
          <h2>Physical Address</h2>

          <input placeholder="Enter Street Address" label="Street Address" id='address' />
          <input placeholder="Enter State" label="State" id='state'/>
          <input placeholder="Enter Zipcode" label="Zipcode" id='zip'/>
          <h2>Contact Info</h2>
          <input placeholder="Enter Full Name" label="Full Name" id='fullName'/>
          <input placeholder="Enter Phone number" label="Phone number" id='phone'/>
          <input placeholder="Confirm Phone number" label="Confirm Phone number" id='phoneConfirm'/>
          <div class="text-center text-lg-start mt-4 pt-2">
            <Button type="submit" class="btn btn-primary btn-lg" onClick={updateInfo}><AiFillCheckCircle/>Update</Button>
          </div>
          <PopupModal show={popupModal} body={modalBody} onHide={() => setPopupModal(false)} />
          <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
      </div>
    </div>
  </div>
  </div>
}
export default PostRegUser;