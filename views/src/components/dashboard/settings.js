import Jumbotron1 from "./jumbotron1";
import UserHeader from "../user_header";
import Footer from "../footer"
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { Modal } from "react-bootstrap";
import { useState } from "react";
import PopupAlert from "./popup_alert";

function Settings (){
    const navigate = useNavigate()

    const [popupShow, popupSetShow] = useState(false)
    const [popupType, popupSetType] = useState(false)
    const [popupText, popupSetText] = useState(false)
    const [popupModal, setPopupModal] = useState(false)
    const [popupModal1, setPopupModal1] = useState(false)
    const [modalBody, setBody] = useState('')
    const [userActive, setActive] = useState()
    const [requestServiceActive, setRSActive] = useState()


    function updateInformation(){
        navigate('/update_user')
    }

    function handleModal(){
        setPopupModal(false)
    }

    function handleModal1(){
        setPopupModal1(false)
    }

    function handleRequest(){
      var newStatus = userActive
      var body = ''
        if(newStatus === 1){
            newStatus = 0
            body = JSON.stringify({
              Active: newStatus,
              PendingApproval: 0
            })
        }
        else{
            newStatus = 1
            body = JSON.stringify({
              Active: newStatus,
              PendingApproval: 1
            })
        }
        fetch('/user/update', 
        {
          method: 'PUT',
          headers: {'Content-Type': 'application/json'},
          body: body
        }).then((response) =>{
            if(!response.ok){
                response.text().then(text => {
                    popupSetShow(true)
                    popupSetType(1)
                    popupSetText(text)
                  })
            }
            else{
                popupSetShow(true)
                popupSetType(2)
                popupSetText('Successful Update!')
            }
        })

        setPopupModal(false)
    }

    async function service(){
        await fetch('user/get', {method: 'GET'}).then(response => response.json())
        .then(json => {
          if (json.Active === 1 && json.PendingApproval === 1){
            setActive(1)
            setRSActive(false)
            setBody('STATUS: Pending Approval. Please wait for DeMarco Disposal to reach out and confirm details.')
          }
          else if(json.Active === 1 && json.PendingApproval === 0){
            setActive(1)
            setRSActive(true)
            setBody('STATUS: ACTIVE. Would you like to cancel your pickups with DeMarco Disposal?')
          }
          else{
            setActive(0)
            setRSActive(true)
            setBody('STATUS: INACTIVE. Would you like to start receiving pickups with Demarco Disposal?')
          }
    })
    setPopupModal(true)
}

function deleteAccount(){
    setBody('Are you sure you would like to remove your account? There is no going back!')
    setPopupModal1(true)
}

async function deactivate(){
    fetch('/user/delete', {method: 'DELETE', headers: {'Content-Type': 'application/json'}}).then((response) =>{
        if(!response.ok){
            response.text().then(text => {
                popupSetShow(true)
                popupSetType(1)
                popupSetText(text)
              })
        }
        else{
            popupSetShow(true)
            popupSetType(2)
            popupSetText('Account Deleted!')
        }
    })
    setPopupModal1(false)
}

return <div>
    <UserHeader/>
    <Jumbotron1 Header = 'Settings' Body = 'Update your information, stop service, or deactivate your account'/>


    <div className="headermatch">
    <Button onClick={updateInformation} variant='primary'>Update Information</Button>
    <Button onClick={service} variant='secondary'>Start or Stop Service</Button>
    <Button onClick={deleteAccount} variant='danger'>Deactivate Account</Button>
    </div>


    <Modal show={popupModal} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header><Modal.Title id="contained-modal-title-vcenter"><h1>Please Update Service Status or Close.</h1></Modal.Title></Modal.Header>
    <Modal.Body>{modalBody}</Modal.Body>
      <Modal.Footer>
        <Button variant='danger' onClick={handleModal}>Close</Button>
        { requestServiceActive ? <Button variant='primary' onClick={handleRequest}>Update</Button> : null }
      </Modal.Footer>
    </Modal>


    <Modal show={popupModal1} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header><Modal.Title id="contained-modal-title-vcenter"><h1>Select DEACTIVATE to deactivate your account.</h1></Modal.Title></Modal.Header>
    <Modal.Body>{modalBody}</Modal.Body>
      <Modal.Footer>
        <Button variant='danger' onClick={handleModal1}>Close</Button>
        <Button variant='primary' onClick={deactivate}>DEACTIVATE</Button>
      </Modal.Footer>
    </Modal>


    <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
    <Footer/>
    </div>
}
export default Settings;