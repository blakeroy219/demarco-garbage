import { Card, Button } from 'react-bootstrap';
import PopupAlert from './popup_alert';
import { useState } from 'react';

//username email phone number  text area for charge
const ClientComponent = (props) => {
        const [popupShow, popupSetShow] = useState(false)
        const [popupType, popupSetType] = useState(false)
        const [popupText, popupSetText] = useState(false)
        const [state, setState] = useState()
        const [showCard, setShowCard] = useState(true)
        
        const email = props.user.Email

        function approveStatus(){
                setState(0)
                changeStatus()
        }

        function updateStatus(){
                setState(2)
                changeStatus()
        }

        function changeStatus(){
                console.log(email)
                fetch('/user/update', 
                {
                  method: 'PUT',
                  headers: {'Content-Type': 'application/json'},
                  body: JSON.stringify({
                    PendingApproval: state,
                    Email: email,
                  }),
                }).then((response) =>{
                  if(!response.ok){
                    response.text().then(text => {
                        popupSetShow(true)
                        popupSetType(1)
                        popupSetText(text)
                    })
                  }
                  else{
                        popupSetShow(true)
                        popupSetType(2)
                        popupSetText('Information Updated Successfully!')
                        setShowCard(false)
                    }
                })
        }

        return(
        <div>
        <Card show={showCard} border='secondary' bg='dark' text='light' style={{ width: '24rem' }} className="mb-2">
                <Card.Header><h2>{props.user.FullName}</h2></Card.Header>
                <Card.Body>
                <Card.Title>{props.user.Email}</Card.Title>
                <Card.Text>
                        {props.user.PhoneNumber}
                </Card.Text>
                </Card.Body>
                <Card.Footer>
                        <Button variant="primary" onClick={approveStatus}>Approve</Button>
                        <Button variant="danger" onClick={updateStatus}>Deny</Button>
                </Card.Footer>
        </Card>
        <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
        </div>
        );
}
export default ClientComponent;
