import { Table } from "react-bootstrap"

function InfoTable() {
    return <div className="headermatch">
<Table variant="dark">
      <thead>
            <tr>
            <th class ="text-center">First Name</th>
            </tr>
        </thead>    
        <tbody>
            <tr>
            <td class ="text-center">Mark</td>
            </tr>
        </tbody>
        <thead>
            <tr>
            <th class ="text-center">Last Name</th>
            </tr>
        </thead>    
        <tbody>
            <tr>
            <td class ="text-center">Bigguy</td>
            </tr>
        </tbody>
        <thead>
            <tr>
            <th class ="text-center">Address</th>
            </tr>
        </thead>    
        <tbody>
            <tr>
            <td class ="text-center">3044 tree parkway dubai senegal</td>
            </tr>
        </tbody>
        <thead>
            <tr>
            <th class ="text-center">Card Info</th>
            </tr>
        </thead>    
        <tbody>
            <tr>
            <td class ="text-center">Mastercard ***9964</td>
            </tr>
        </tbody>
    </Table>
    </div>
    
}
export default InfoTable;