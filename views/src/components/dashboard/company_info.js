import Header from "../company_header"
import Footer from "../footer"
import Jumbotron1 from "./jumbotron1"
import InfoTable from "./info_table";


function CompanyInfo() {
    return <div>
        <Header/>
        <Jumbotron1 className='justify-content-center' Header = 'Welcome to the Company Info Page' Body = 'This is your info'/>
        <InfoTable/>
        <Footer/>
    </div>
    }
export default CompanyInfo