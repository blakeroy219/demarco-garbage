
import PopupAlert from "./popup_alert";
import CompanyHeader from "../company_header";
import { useState } from "react";
import Footer from "../footer";
import ClientComponent from "./client_component"

function PostRegComp()  {
    const [popupShow, popupSetShow] = useState(false)
    const [popupType, popupSetType] = useState(false)
    const [popupText, popupSetText] = useState(false)
    const [clients, clientSetText] = useState(null)

    if (!clients) loadPendingClients()

  function loadPendingClients(){
    fetch('/company/getClients', 
    {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        PendingApproval: 1, 
      }),
    }).then((response) =>{
      if(!response.ok){
        response.text().then(text => {
          popupSetShow(true)
          popupSetType(1)
          popupSetText(text)
        })
      }
      else{
        return response.json()
        .then((data) => {
          console.log(data)
          var client = []
          for(let i = 0; i < data.length; i++){
  
            client.push(<p><ClientComponent user = {data[i]} /></p>)
          }
          clientSetText(client)
        })
      }
    })
  }



return <div>
<CompanyHeader />
<div class="container-fluid bg-dark text-light">
  <div class="row d-flex justify-content-center align-items-center h-100">
    <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
    <h1>Pending Approval</h1>
        <div class="text-center text-lg-start mt-4 pt-2">
        </div>
        {clients}
        <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
    </div>
  </div>
</div>
<Footer />
</div>
}
export default PostRegComp;