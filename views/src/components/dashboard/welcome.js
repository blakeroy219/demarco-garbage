import {Button} from 'react-bootstrap';
import Header from "../header"
import Footer from "../footer"
import Jumbotron1 from "./jumbotron1"
import {AiOutlineForm} from "react-icons/ai"
import { RiLoginCircleLine } from "react-icons/ri"
import {useNavigate } from 'react-router-dom';


function Welcome() {

  const navigate = useNavigate();

  const handleregisterRoute = () =>{ 
      navigate('/register');
  }
  const handleloginRoute = () =>{ 
      navigate('/login');
  }

    return<div>
        <Header/>
        <Jumbotron1 className='justify-content-center' Header = 'DeMarco Disposal' Body = 'Adirondack Local Waste Management'/>
        <body style={{backgroundColor: "#212121"}}>
        <Button className='center' variant="primary" onClick={handleregisterRoute}> <RiLoginCircleLine/>Sign Up</Button>
        <Button className='center' variant="primary" onClick={handleloginRoute}> <AiOutlineForm/> Login</Button>
        </body>
        <Footer/> 
        </div>   
  }

export default Welcome