import UserHeader from "../user_header"
import Footer from "../footer"
import Jumbotron1 from "./jumbotron1"
import {Button} from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import { Modal } from "react-bootstrap"
import { useState } from "react"
import PopupAlert from "./popup_alert"


function UserWelcome(props) {
    const navigate = useNavigate()
    const [popupShow, popupSetShow] = useState(false)
    const [popupType, popupSetType] = useState(false)
    const [popupText, popupSetText] = useState(false)
    const [popupModal, setPopupModal] = useState(false)
    const [modalBody, setBody] = useState('')
    
    var Email = 'Welcome ' + props.Email + '!';

    function addCardPage(){
        navigate("/card", {state:{method: 'POST'}})
    }
    function updateCardPage(){
        navigate("/card", {state:{method: 'PUT'}})
    }
    function deleteCard(){
        setBody("Are you sure you wish to delete your payment method?")
        setPopupModal(true)
    }

    async function hardDeleteCard(){
        setPopupModal(false)
        fetch('/user/payments', {method: 'DELETE', headers: {'Content-Type': 'application/json'}}).then((response) =>{
            if(!response.ok){
                response.text().then(text => {
                    popupSetShow(true)
                    popupSetType(1)
                    popupSetText(text)
                  })
            }
            else{
                popupSetShow(true)
                popupSetType(2)
                popupSetText('Account Deleted!')
            }
        })
    }

    function handleModal(){
        setPopupModal(false)
    }

    return <div> 
        <UserHeader Email = {props.Email}/>
        <Jumbotron1 Header = {Email} Body = 'Here you can update your card information, view recent payments, and update your profile.' />
        <div className="headermatch">
        {' '}<Button variant="primary" onClick={addCardPage}>Add Card</Button>{' '}
        {' '}<Button variant="success" onClick={updateCardPage}>Edit Card</Button>{' '}
        {' '}<Button variant="danger" onClick={deleteCard}>Delete Card</Button>{' '}
        </div>


        <Modal show={popupModal} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
    <Modal.Header><Modal.Title id="contained-modal-title-vcenter"><h1>Select Delete to delete your stored payment method.</h1></Modal.Title></Modal.Header>
    <Modal.Body>{modalBody}</Modal.Body>
      <Modal.Footer>
        <Button variant='danger' onClick={handleModal}>Close</Button>
        <Button variant='primary' onClick={hardDeleteCard}>Delete</Button>
      </Modal.Footer>
    </Modal>

    <PopupAlert show = {popupShow} Type = {popupType} Text = {popupText} onHide={() => popupSetShow(false)} />

        <Footer/>
    </div>
}

export default UserWelcome