import { Modal, Button } from "react-bootstrap"
import { useState } from "react"
import PopupAlert from "./popup_alert"

function PopupModal(props){
    const [popupShow, popupSetShow] = useState(false)
    const [popupType, popupSetType] = useState(false)
    const [popupText, popupSetText] = useState(false)

    async function handleRequest(){
        await fetch('/user/update', 
        {
          method: 'PUT',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify(props.body)
        }
        ).then((response) =>{
            if(!response.ok){
                response.text().then(text => {
                popupSetShow(true)
                popupSetType(1)
                popupSetText(text)
                props.onHide()
                })
            }
            else{
                popupSetShow(true)
                popupSetType(2)
                popupSetText('Information Updated Successfully!')
                props.onHide()
            }
        })  
    }

      return <div>
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          <h1>Everything Looks Good?</h1>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
          <h3>Contact Info</h3>
          <h5>Full Name: {props.body.FullName}</h5>
          <h5>Address: {props.body.Address},</h5>
          <h5>Phone Number: {props.body.PhoneNumber},</h5>
          <h5>State: {props.body.State},</h5>
          <h5>Zip Code: {props.body.ZipCode}</h5>  
      </Modal.Body>
      <Modal.Footer>
        <Button variant='danger' onClick={props.onHide}>Close</Button>
        <Button variant='primary' onClick={handleRequest}>Update</Button>
      </Modal.Footer>
    </Modal>
    <div>
    <PopupAlert show = {popupShow} type = {popupType} text = {popupText} onHide={() => popupSetShow(false)} />
    </div>
      </div>
    }
export default PopupModal