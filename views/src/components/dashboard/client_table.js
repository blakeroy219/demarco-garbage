import * as React from 'react'
import { DataGrid, } from '@mui/x-data-grid';
import { makeStyles } from '@mui/styles';

  const useStyles = makeStyles({
    root: {
      "& .super-app-theme--header": {
        backgroundColor: "#f2f2f2",
      },
    }
  });


 const ClientTable = (props) => {

  const classes = useStyles();

  return (
    <div>
      <div style={{ height: 300, width: "100%" }} className={classes.root}>
      <DataGrid
        classes={{root: classes.muiGridRoot,}}
        rowHeight={60}
        sx={{
          m: 2,
          boxShadow: 2,
          border: 2,
          borderColor: 'primary.light',
          '& .MuiDataGrid-cell:hover': {color: 'primary.main',},
        }}
  rows={props.clients}
  columns={[{ field: 'id', hide: true }, { field: 'FullName' }, { field: 'Email' }, { field: 'PhoneNumber' }, { field: 'BaseCharge', editable: true }]}
  pageSize={5}
  rowsPerPageOptions={[5]}
  checkboxSelection/>
  </div>
    </div>
     );
  }
export default ClientTable;
