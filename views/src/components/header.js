import { Navbar, Nav, Dropdown, Container} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap'
import { RiHome8Line, RiLoginCircleLine } from "react-icons/ri"
import {AiOutlineForm} from "react-icons/ai"
import { MdManageAccounts} from "react-icons/md"
import Sticky from 'react-stickynode';

const Header = () => {
    return <div className='headermatch'>
        <Sticky enabled={true} top={0} bottomBoundary={1200}>
        <Navbar className='rcorners1' style={{backgroundColor: "#B4B8B4"}} variant="light">
        <Container fluid>
                <Navbar.Brand>
                    <img src='DemarcoDisposalLogo.png' alt=''/>
                </Navbar.Brand>
                    <Nav className="me-auto">
                        <LinkContainer to="/">
                            <Nav.Link><RiHome8Line/>Home</Nav.Link>
                        </LinkContainer>
                    </Nav>
                    <Nav className= "justify-content-right">
                    <Dropdown>
                            <Dropdown.Toggle variant="white" id="dropdown-link">
                            <MdManageAccounts/>Account</Dropdown.Toggle>
                        <Dropdown.Menu>
                            <Dropdown.Item href="/login"><RiLoginCircleLine/> Login</Dropdown.Item>
                            <Dropdown.Item href="/register"><AiOutlineForm/> Register</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    </Nav> 
                    </Container>     
        </Navbar>
        </Sticky>
    </div>

}

export default Header