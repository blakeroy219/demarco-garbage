import {Button, OverlayTrigger, Popover } from 'react-bootstrap';
import { RiCopyrightFill } from "react-icons/ri"
import {FiPhoneForwarded} from "react-icons/fi"


const Footer = () => {
    return <div>
        <div style={{textAlignVertical: "center",textAlign: "center",backgroundColor: "black",}}class="container-fluid text-light p-5">
        <div class="container p-5">
            <h5 class="display-1">{"Find Us Here                                                                                    "}
            <OverlayTrigger trigger="click" placement="right" overlay={popover}>
            <Button type="button" class="btn btn-info" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="right" data-bs-content="Right popover">
            <FiPhoneForwarded/>  Contact Us</Button></OverlayTrigger></h5>
            <hr />
            <p></p>
            <div className="mb-2">
            </div>
        </div>
        <div class="d-flex flex-column flex-md-row text-center text-md-start justify-content-between py-4 px-4 px-xl-5 bg-gray">
        <div class="text-white mb-3 mb-md-0">Copyright <RiCopyrightFill/> 2022. All rights reserved.</div>
        </div>
    </div>
    </div>    
}

const popover = (
    <Popover id="popover-basic">
    <Popover.Header as="h3">Contact Us</Popover.Header>
    <Popover.Body>
    Phone Number
    Email
    </Popover.Body>
    </Popover>
);
export default Footer