import { Navbar, Container, Nav, Dropdown  } from 'react-bootstrap';
import {GearFill, CreditCard, PersonCircle} from 'react-bootstrap-icons';
import { RiHome8Line } from "react-icons/ri"
import { MdManageAccounts } from "react-icons/md"
import Sticky from "react-stickynode"
import { Link } from 'react-router-dom';

const UserHeader = (props) => {
    var email = props.Email

    return <div className='headermatch'> 
        <Sticky enabled={true} top={0} bottomBoundary={1200}>
        <Navbar className='rcorners1' style={{backgroundColor: "#B4B8B4"}} variant="light">
            <Container>
                <Navbar.Brand>
                    <img src='DemarcoDisposalLogo.png' alt=''/>
                </Navbar.Brand>
                    <Nav className="me-auto">
                            <Nav.Link as={Link} to='/user' state={{Email: email}}>
                            <RiHome8Line/> Home </Nav.Link>

                            <Nav.Link as={Link} to='/payment' state={{Email: email}} ><CreditCard/> Payments </Nav.Link>
                    </Nav>
                    <Nav className="justify-content-right">
                        <Dropdown>
                            <Dropdown.Toggle variant="white" id="dropdown-link">
                            <MdManageAccounts/>Account</Dropdown.Toggle>
                        <Dropdown.Menu>
                            <Dropdown.Item href="/logout"><PersonCircle/> Logout </Dropdown.Item>
                            <Dropdown.Item href="/settings"><GearFill/> Settings </Dropdown.Item>
                        </Dropdown.Menu>
                        </Dropdown>
                    </Nav>
            </Container>
        </Navbar>
        </Sticky>
    </div>
}

export default UserHeader