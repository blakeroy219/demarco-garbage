import CompanyWelcome from "./dashboard/company_welcome"
import { useLocation } from "react-router-dom";
import { useState } from "react";

function Company() {
  const location = useLocation();
  var passedemail = location.state.Email

  const[email, setEmail] = useState(passedemail)

  if (!email){
    getUser()
  }

  async function getUser(){
    await fetch('user/get', {method: 'GET'}).then(response => response.json())
    .then(json => {
        setEmail(json.Email)
    })
  }

    return (
      <div className='User'>
        <CompanyWelcome Email ={email} />
      </div>
    );
  }
  
  export default Company;