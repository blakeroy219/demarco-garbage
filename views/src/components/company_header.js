import { Navbar, Container, Nav, Dropdown } from 'react-bootstrap';
import { RiHome8Line, RiBillLine } from "react-icons/ri"
import { MdManageAccounts } from "react-icons/md"
import Sticky from 'react-stickynode';
import { Link } from 'react-router-dom';
import { PersonCircle } from 'react-bootstrap-icons';

const CompanyHeader = (props) => {
    var email = props.Email
    return <div className='headermatch'>
        <Sticky enabled={true} top={0} bottomBoundary={1200}>
        <Navbar className='rcorners1' style={{backgroundColor: "#B4B8B4"}} variant="light">
            <Container>
                <Navbar.Brand>
                    <img src='DemarcoDisposalLogo.png' alt=''/>
                </Navbar.Brand>
                <Nav className="me-auto">
                            <Nav.Link as={Link} to='/company' state={{Email: email}}>
                            <RiHome8Line/> Home </Nav.Link>

                            <Nav.Link as={Link} to='/management' state={{Email: email}} ><RiBillLine/> Client Management </Nav.Link>
                            <Nav.Link as={Link} to='/pending_clients'state={{Email: email}}><MdManageAccounts/>Pending Clients</Nav.Link>
                    </Nav>
                    <Nav className="justify-content-right">
                    <Dropdown>
                            <Dropdown.Toggle variant="white" id="dropdown-link">
                            <MdManageAccounts/>Account</Dropdown.Toggle>
                        <Dropdown.Menu>
                            <Dropdown.Item href="/logout"><PersonCircle/> Logout </Dropdown.Item>
                        </Dropdown.Menu>
                        </Dropdown>
                    </Nav>
            </Container>
        </Navbar>
        </Sticky>
    </div>
}

export default CompanyHeader